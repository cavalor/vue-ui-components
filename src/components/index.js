import Lightbox from './Lightbox/Lightbox';
import LightboxPreview from './Lightbox/LightboxPreview';
import Icon from './Icon/Icon';

export default {
  install(Vue) {
    Vue.component('lightbox', Lightbox);
    Vue.component('lightbox-preview', LightboxPreview);
    Vue.component('ui-icon', Icon);
  }
};
