import Vue from 'vue';

let init = false;
const elements = new Map();
const galleries = new Map();
const state = {
  show: false
};

export default class LightboxStore {

  constructor() {
    throw Error('[ LightboxStore ] Store contain only statics methods and properties, please use this as static class.');
  }

  static get state() {
    return state;
  }

  /**
   * Register an elements in elements collection
   * @param elementId
   * @param element
   */
  static registerElement(elementId, element) {
    elements.set(elementId, element);
  }

  /**
   * Register a gallery in galleries collection
   * @param galleryId
   */
  static registerGallery(galleryId) {
    galleries.set(galleryId, []);
    console.log(galleries);
  }

  /**
   * Link an element with a gallery
   * @param galleryId
   * @param elementId
   */
  static addElementInGallery(galleryId, elementId) {
    if (!galleries.has(galleryId)) {
      throw Error('[ LightboxStore ] Trying to add an element in an unregistered gallery.');
    }
    galleries.get(galleryId).push(elementId);
  }

  /**
   * Set current element
   * @param elementId
   */
  static setCurrentElement(elementId) {
    if (!elements.has(elementId)) {
      throw Error('[ LightboxStore ] Trying to set as currently selected an unregistered element.');
    }
    state.currentElementId = elementId;
    state.current = LightboxStore.getCurrentElement();
  }

  /**
   * Get a gallery from her id
   * @param galleryId
   * @returns {V}
   */
  static getGallery(galleryId) {
    return galleries.get(galleryId);
  }

  /**
   * Get an element from his id
   * @param elementId
   * @returns {V}
   */
  static getElement(elementId) {
    return elements.get(elementId);
  }

  /**
   * Get the current element
   * @returns {V}
   */
  static getCurrentElement() {
    return LightboxStore.getElement(state.currentElementId);
  }

  static show(elementId = null) {
    if (!init) {
      LightboxStore.createLightbox();
    }
    if (elementId) {
      LightboxStore.setCurrentElement(elementId);
    }
    state.show = true;
  }

  /**
   * Reset state
   */
  static hide() {
    state.currentElementId = null;
    state.show = false;
    state.current = null;
  }

  /**
   * Render off-document and append afterwards
   */
  static createLightbox() {
    const lightbox = new Vue({ template: '<lightbox />' }).$mount();
    document.querySelector('#app').appendChild(lightbox.$el);
    init = true;
  }

}
