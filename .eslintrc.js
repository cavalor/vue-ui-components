module.exports = {
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module"
  },
  extends: "airbnb-base",
  // required to lint *.vue files
  plugins: [
    "html"
  ],
  // check if imports actually resolve
  "settings": {
    "import/resolver": {
      "webpack": {
        "config": "build/webpack.base.conf.js"
      }
    }
  },
  // add your custom rules here
  "rules": {
    // don"t require .vue extension when importing
    "import/extensions": ["error", "always", {
      "js": "never",
      "vue": "never"
    }],
    // allow debugger during development
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    // personal rules
    "no-param-reassign": 0,
    "comma-dangle": 0,
    "no-plusplus": 0,
    "no-shadow": 0,
    "quote-props": 0,
    "import/prefer-default-export": 0,
    "space-before-function-paren": 0,
    "no-console": 0,
    "no-undef": 0,
    "no-prototype-builtins": 0,
    "no-restricted-syntax": 0,
    "global-require": 0,
    "no-underscore-dangle": 0,
    "no-unused-expressions": ["error", { "allowTernary": true }],
    "no-new": 0,
    "array-callback-return": 0
  }
};
